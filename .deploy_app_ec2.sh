#!/bin/bash

#Get servers list
set -f
SERVER_PORT=8800
string=$DEPLOY_SERVER
array=(${string//,/ })

#Iterate servers for deploy and pull last commit
for i in "${!array[@]}"; do
  echo "Deploying information to EC2 and Gitlab"
  echo "Deploy project on server ${array[i]}"
  scp -rv ./build ubuntu@${array[i]}:/var/www
  ssh ubuntu@${array[i]} 'bash -s' < ./script_run_on_ec2.sh
  echo "Server running on http://${DEPLOY_SERVER}:${SERVER_PORT}"
done
