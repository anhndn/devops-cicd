#!/bin/bash
# Purpose: Run command to prepare sources & start server on EC2 
# ----------------------------------------------------
SERVER_PORT=8800
# Change working directory to source dir
cd  /var/www/build && ls -l
# Kill running server
kill -9 `ps -ef |grep http.server |grep ${SERVER_PORT} |awk '{print $2}'`
# Start server
python3 -m http.server ${SERVER_PORT} > /dev/null 2>&1 &